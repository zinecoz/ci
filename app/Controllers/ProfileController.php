<?php 
namespace App\Controllers;  
use CodeIgniter\Controller;
  
class ProfileController extends Controller
{
    public function index()
    {
        $session = session();
        $data['name'] = $session->get('name');
        echo view('home', $data);
    }

    public function logout()
    {
        $session = session();
        $ses_data = [
            'id',
            'name',
            'email',
            'isLoggedIn'
        ];
        $session->remove($ses_data);
        return redirect()->to('/');
    }
}