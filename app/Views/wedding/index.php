<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Wedding Achmad Muhammad & Hamilatul Hasanah</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Template by FREEHTML5.CO" />
	<meta name="keywords" content="free html5, free template, free bootstrap, html5, css3, mobile first, responsive" />
	<meta name="author" content="FREEHTML5.CO" />

  <!-- 
	//////////////////////////////////////////////////////

	FREE HTML5 TEMPLATE 
	DESIGNED & DEVELOPED by FREEHTML5.CO
		
	Website: 		http://freehtml5.co/
	Email: 			info@freehtml5.co
	Twitter: 		http://twitter.com/fh5co
	Facebook: 		https://www.facebook.com/fh5co

	//////////////////////////////////////////////////////
	 -->

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<link href='https://fonts.googleapis.com/css?family=Work+Sans:400,300,600,400italic,700' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Sacramento" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="/wedding/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="/wedding/css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="/wedding/css/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="/wedding/css/magnific-popup.css">

	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="/wedding/css/owl.carousel.min.css">
	<link rel="stylesheet" href="/wedding/css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="/wedding/css/style.css">

	<!-- Modernizr JS -->
	<script src="/wedding/js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
		
	<div class="fh5co-loader"></div>
	
	<div id="page">
	<nav class="fh5co-nav" role="navigation">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div id="fh5co-logo"><a href="#">Undangan Pernikahan</a></div>
				</div>
			</div>
		</div>
	</nav>

	<header id="fh5co-header" class="fh5co-cover" role="banner" style="background-image:url(/wedding/images/img_bg_2.jpg);" data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center">
					<div class="display-t">
						<div class="display-tc animate-box" data-animate-effect="fadeIn">
							<h1>Soleh &amp; Mila</h1>
							<h2>Kami akan menikah</h2>
							<div class="simply-countdown simply-countdown-one"></div>
							<p><a href="https://calendar.google.com/calendar/event?action=TEMPLATE&tmeid=MXI2Z2Q4bmI1cm81bjVlb2d1aXEwamJib2ogemFpbmFsLnRlLnV0bUBt&tmsrc=zainal.te.utm%40gmail.com" class="btn btn-default btn-sm">Atur Pengingat</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>

	<div id="fh5co-couple">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
					<h2>Assalamualikum Wr. Wb.</h2>
					<p>Dengan rahmat dan rida Allah SWT, Kami bermaksud mengundang Bapak/Ibu/Saudara ke acara pernikahan kami. InsyaAllah acara pernikahan ini akan berlangsung pada:</p>
					<p>Senin, 27 Februari 2023</p>
				</div>
			</div>
			<div class="couple-wrap animate-box">
				<div class="couple-half">
					<div class="groom">
						<img src="/wedding/images/groom.jpg" alt="groom" class="img-responsive">
					</div>
					<div class="desc-groom">
						<h3>Nurus Soleh</h3>
						<p>Putra ke-2 dari Bapak Achmad dan Ibu Aisyah</p>
					</div>
				</div>
				<p class="heart text-center"><i class="icon-heart2"></i></p>
				<div class="couple-half">
					<div class="bride">
						<img src="/wedding/images/bride.jpg" alt="groom" class="img-responsive">
					</div>
					<div class="desc-bride">
						<h3>Hamilatul Hasanan</h3>
						<p>Putri ke-2 dari Bapak H Munir dan Ibu Hj Sittiyah (Alm)</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="fh5co-event" class="fh5co-bg" style="background-image:url(/wedding/images/img_bg_3.jpg);">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
					<span>Acara Spesial Kami</span>
					<h2>Agenda Pernikahan</h2>
				</div>
			</div>
			<div class="row">
				<div class="display-t">
					<div class="display-tc">
						<div class="col-md-10 col-md-offset-1">
							<div class="col-md-6 col-sm-6 text-center">
								<div class="event-wrap animate-box">
									<h3>Akad Nikah</h3>
									<div class="event-col">
										<i class="icon-clock"></i>
										<span>10:00 WIB</span>
										<span>Selesai</span>
									</div>
									<div class="event-col">
										<i class="icon-calendar"></i>
										<span>Senin, 27</span>
										<span>Februari 2023</span>
									</div>
									<p>Dsn. Sendang Desa Bangsah Kec. Sreseh Kab. Sampang, Jawa Timur</p>
									<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3958.5834276349806!2d113.06658684983684!3d-7.174056872365218!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd78b8cc458cbcd%3A0xa31f39df26afd866!2sMasjid%20Sendang!5e0!3m2!1sid!2sid!4v1674312686699!5m2!1sid!2sid" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
								</div>
							</div>
							<div class="col-md-6 col-sm-6 text-center">
								<div class="event-wrap animate-box">
									<h3>Resepsi</h3>
									<div class="event-col">
										<i class="icon-clock"></i>
										<span>07:00 WIB</span>
										<span>Selesai</span>
									</div>
									<div class="event-col">
										<i class="icon-calendar"></i>
										<span>Senin, 27</span>
										<span>Februari 2023</span>
									</div>
									<p>Dsn. Sendang Desa Bangsah Kec. Sreseh Kab. Sampang, Jawa Timur</p>
									<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3958.5834276349806!2d113.06658684983684!3d-7.174056872365218!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd78b8cc458cbcd%3A0xa31f39df26afd866!2sMasjid%20Sendang!5e0!3m2!1sid!2sid!4v1674312686699!5m2!1sid!2sid" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--
	<div id="fh5co-couple-story">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
					<span>We Love Each Other</span>
					<h2>Our Story</h2>
					<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-md-offset-0">
					<ul class="timeline animate-box">
						<li class="animate-box">
							<div class="timeline-badge" style="background-image:url(/wedding/images/couple-1.jpg);"></div>
							<div class="timeline-panel">
								<div class="timeline-heading">
									<h3 class="timeline-title">First We Meet</h3>
									<span class="date">December 25, 2015</span>
								</div>
								<div class="timeline-body">
									<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
								</div>
							</div>
						</li>
						<li class="timeline-inverted animate-box">
							<div class="timeline-badge" style="background-image:url(/wedding/images/couple-2.jpg);"></div>
							<div class="timeline-panel">
								<div class="timeline-heading">
									<h3 class="timeline-title">First Date</h3>
									<span class="date">December 28, 2015</span>
								</div>
								<div class="timeline-body">
									<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
								</div>
							</div>
						</li>
						<li class="animate-box">
							<div class="timeline-badge" style="background-image:url(/wedding/images/couple-3.jpg);"></div>
							<div class="timeline-panel">
								<div class="timeline-heading">
									<h3 class="timeline-title">In A Relationship</h3>
									<span class="date">January 1, 2016</span>
								</div>
								<div class="timeline-body">
									<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
								</div>
							</div>
						</li>
			    	</ul>
				</div>
			</div>
		</div>
	</div>
	-->

	<div id="fh5co-gallery" class="fh5co-section-gray">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
					<span>Dokumentasi</span>
					<h2>Galery Pernikahan</h2>
					<p>Akan tampil ketika acara sudah selesai</p>
				</div>
			</div>
			<div class="row row-bottom-padded-md">
				<div class="col-md-12">
					<ul id="fh5co-gallery-list">
						<li class="one-third animate-box" data-animate-effect="fadeIn" style="background-image: url(/wedding/images/gallery-1.jpg); "> 
							<a href="/wedding/images/gallery-1.jpg">
								<div class="case-studies-summary">
									<span>Belum Upload</span>
									<h2>Foto Pengantin</h2>
								</div>
							</a>
						</li>
						<li class="one-third animate-box" data-animate-effect="fadeIn" style="background-image: url(/wedding/images/gallery-2.jpg); ">
							<a href="#" class="color-2">
								<div class="case-studies-summary">
									<span>Belum Upload</span>
									<h2>Foto Keluarga</h2>
								</div>
							</a>
						</li>
						<li class="one-third animate-box" data-animate-effect="fadeIn" style="background-image: url(/wedding/images/gallery-3.jpg); ">
							<a href="#" class="color-3">
								<div class="case-studies-summary">
									<span>Belum Upload</span>
									<h2>Foto Sahabat</h2>
								</div>
							</a>
						</li>
					</ul>		
				</div>
			</div>
		</div>
	</div>
<!--
	<div id="fh5co-counter" class="fh5co-bg fh5co-counter" style="background-image:url(/wedding/images/img_bg_5.jpg);">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="display-t">
					<div class="display-tc">
						<div class="col-md-3 col-sm-6 animate-box">
							<div class="feature-center">
								<span class="icon">
									<i class="icon-users"></i>
								</span>

								<span class="counter js-counter" data-from="0" data-to="500" data-speed="5000" data-refresh-interval="50">1</span>
								<span class="counter-label">Estimated Guest</span>

							</div>
						</div>
						<div class="col-md-3 col-sm-6 animate-box">
							<div class="feature-center">
								<span class="icon">
									<i class="icon-user"></i>
								</span>

								<span class="counter js-counter" data-from="0" data-to="1000" data-speed="5000" data-refresh-interval="50">1</span>
								<span class="counter-label">We Catter</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 animate-box">
							<div class="feature-center">
								<span class="icon">
									<i class="icon-calendar"></i>
								</span>
								<span class="counter js-counter" data-from="0" data-to="402" data-speed="5000" data-refresh-interval="50">1</span>
								<span class="counter-label">Events Done</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 animate-box">
							<div class="feature-center">
								<span class="icon">
									<i class="icon-clock"></i>
								</span>

								<span class="counter js-counter" data-from="0" data-to="2345" data-speed="5000" data-refresh-interval="50">1</span>
								<span class="counter-label">Hours Spent</span>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
-->

	<div id="fh5co-testimonial">
		<div class="container">
			<div class="row">
				<div class="row animate-box">
					<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
						<span>Doa Terbaik</span>
						<h2>Doa Untuk Mempelai</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 animate-box">
						<div class="wrap-testimony">
							<div class="owl-carousel-fullwidth">
								<div class="item">
									<div class="testimony-slide active text-center">
										<figure>
											<img src="/wedding/images/couple-1.jpg" alt="user">
										</figure>
										<span>Al-Qur'an <a href="#" class="twitter">Ar Rum ayat 12</a></span>
										<blockquote>
											<p>"Dan di antara tanda-tanda kekuasaan-Nya yang Agung, Dia menciptakan bagimu berpasang-pasangan dari jenismu sendiri, agar kamu cenderung dan tenteram dengannya, dan Dia menjadikan di antara kamu rasa cinta dan kasih sayang. Sesungguhnya pada yang demikian itu benar-benar terdapat tanda-tanda (kebesaran Allah) bagi orang-orang yang berpikir."</p>
										</blockquote>
									</div>
								</div>
								<div class="item">
									<div class="testimony-slide active text-center">
										<figure>
											<img src="/wedding/images/couple-2.jpg" alt="user">
										</figure>
										<span>Al-Hadist <a href="#" class="twitter">HR. Abu Dawud no. 2130</a></span>
										<blockquote>
											<p>"Semoga Allah memberkahimu di waktu bahagia dan memberkahimu di waktu susah, serta semoga Allah mempersatukan kalian berdua dalam kebaikan."</p>
										</blockquote>
									</div>
								</div>
								<div class="item">
									<div class="testimony-slide active text-center">
										<figure>
											<img src="/wedding/images/couple-3.jpg" alt="user">
										</figure>
										<span>Al-Qur'an <a href="#" class="twitter">Al Furqan ayat 12</a></span>
										<blockquote>
											<p>"Wahai Tuhan kami, anugerahkanlah kepada kami istri-istri dan keturunan kami sebagai penyenang hati, dan jadikanlah kami imam (Pemimpin) bagi orang-orang yang bertakwa."</p>
										</blockquote>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div id="fh5co-services" class="fh5co-section-gray">
		<div class="container">
			
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					<h2>Harapan Kami</h2>
					<p>Kami sangat berharap Bapak/Ibu/Saudara/Saudari berkenan menghadiri acara pernikahan ini . Terima kasih banyak atas perhatian yang diberikan.</p>
				</div>
			</div>
<!--
			<div class="row">
				<div class="col-md-6">
					<div class="feature-left animate-box" data-animate-effect="fadeInLeft">
						<span class="icon">
							<i class="icon-calendar"></i>
						</span>
						<div class="feature-copy">
							<h3>We Organized Events</h3>
							<p>Facilis ipsum reprehenderit nemo molestias. Aut cum mollitia reprehenderit. Eos cumque dicta adipisci architecto culpa amet.</p>
						</div>
					</div>

					<div class="feature-left animate-box" data-animate-effect="fadeInLeft">
						<span class="icon">
							<i class="icon-image"></i>
						</span>
						<div class="feature-copy">
							<h3>Photoshoot</h3>
							<p>Facilis ipsum reprehenderit nemo molestias. Aut cum mollitia reprehenderit. Eos cumque dicta adipisci architecto culpa amet.</p>
						</div>
					</div>

					<div class="feature-left animate-box" data-animate-effect="fadeInLeft">
						<span class="icon">
							<i class="icon-video"></i>
						</span>
						<div class="feature-copy">
							<h3>Video Editing</h3>
							<p>Facilis ipsum reprehenderit nemo molestias. Aut cum mollitia reprehenderit. Eos cumque dicta adipisci architecto culpa amet.</p>
						</div>
					</div>

				</div>

				<div class="col-md-6 animate-box">
					<div class="fh5co-video fh5co-bg" style="background-image: url(/wedding/images/img_bg_3.jpg); ">
						<a href="https://vimeo.com/channels/staffpicks/93951774" class="popup-vimeo"><i class="icon-video2"></i></a>
						<div class="overlay"></div>
					</div>
				</div>
			</div>
									-->

			
		</div>
	</div>
	


	<div id="fh5co-started" class="fh5co-bg" style="background-image:url(/wedding/images/img_bg_4.jpg);">
		<div class="overlay"></div>
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					<h2>Wassalamualaikum Wr. Wb.</h2>
					<p>Dari Kami yang berbahagia Keluarga Nurus Soleh & Hamilatul Hasanah</p>
				</div>
			</div>
		</div>
	</div>

	<footer id="fh5co-footer" role="contentinfo">
		<div class="container">
			<div class="row copyright">
				<div class="col-md-12 text-center">
					<p>
						<small class="block">&copy; 2023 Asyrof Project. All Rights Reserved.</small> 
						<small class="block">Designed by FREEHTML5.co & Asyrof Project</small>
					</p>
					<p>
						<ul class="fh5co-social-icons">
							<li><a href="#"><i class="icon-twitter"></i></a></li>
							<li><a href="#"><i class="icon-facebook"></i></a></li>
							<li><a href="#"><i class="icon-linkedin"></i></a></li>
							<li><a href="#"><i class="icon-dribbble"></i></a></li>
						</ul>
					</p>
				</div>
			</div>
		</div>
	</footer>
	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
	
	<!-- jQuery -->
	<script src="/wedding/js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="/wedding/js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="/wedding/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="/wedding/js/jquery.waypoints.min.js"></script>
	<!-- Carousel -->
	<script src="/wedding/js/owl.carousel.min.js"></script>
	<!-- countTo -->
	<script src="/wedding/js/jquery.countTo.js"></script>

	<!-- Stellar -->
	<script src="/wedding/js/jquery.stellar.min.js"></script>
	<!-- Magnific Popup -->
	<script src="/wedding/js/jquery.magnific-popup.min.js"></script>
	<script src="/wedding/js/magnific-popup-options.js"></script>

	<!-- // <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/0.0.1/prism.min.js"></script> -->
	<script src="/wedding/js/simplyCountdown.js"></script>
	<!-- Main -->
	<script src="/wedding/js/main.js"></script>

	<script>
    //var d = new Date(new Date().getTime() + 200 * 120 * 120 * 2000);
	var d = new Date("February 27, 2023 10:00:00");

    // default example
    simplyCountdown('.simply-countdown-one', {
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate()
    });

    //jQuery example
    $('#simply-countdown-losange').simplyCountdown({
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate(),
        enableUtc: false
    });
</script>

	</body>
</html>

